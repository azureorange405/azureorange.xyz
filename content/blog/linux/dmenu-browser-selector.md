---
title: "Dmenu script: browser selector"
date: 2022-09-28T15:27:30+02:00
draft: true
---

# dmenu scripts

## dm-browser aka xdg-select

I use several different browsers for different aspects of my life, this reduces tracking by browser fingerprinting and I just see it as "internet best practice".

The only problem I encountered was xdg-open, which always opens urls in the wrong browser. 
So I thought why not set a shell script, that lets you choose a browser, as default browser.

This is [dm-browser](https://gitlab.com/azureorange404/dmenu-scripts/).

Every time xdg-open calls the script, dmenu is launched and provides you with a selection of profiles you set in the script.

I use a selfmade [dmenu patch]() to prevent the Escape key from closing dmenu (by using the flag -no-escape), because of xdg-open redirecting to a random browser if the script failes.

