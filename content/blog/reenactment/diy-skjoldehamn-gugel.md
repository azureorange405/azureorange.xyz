---
title: "DIY: Sewing a Skjoldehamn Hood"
date: 2023-08-10T00:15:47+02:00
draft: false
---




## Pattern

The original finding of the Skjoldehamn hood had one measuring 65 x 65 cm.
The original had a cord attached to the hood, so it could be tightened at the back.
I will add one in the future, but just did not have the time right now.
[Enter something about the original finding by Løvlid] (Løvlid, 2011).

To get the width of the hood (A), I took half the circumference of my head and added 5 cm for an easier fit.
So the main square (1) measures 67 x 67 cm.

The back piece (2) is a square of 35 x 35 cm and the front piece (3) is a square of 30 x 30 cm.
So with (C) being 35 cm in length, the back portion of the hood (B) measures 32 cm respectively.

The cutout for the face (E) measures half the circumference of my face plus 2 or 3 cm spare.
This gives me a cutout of 30 cm in length.
On top of this cutout I left approx. 2 cm intact and between the face cutout and the one for the front piece I left approx. 1.5 cm.

The main square is cut at an angle to match the final front height of the hood, so the height (F) of the angle cut was approx. 3.5 cm.

![skjoldehamn hood pattern](./hood-pattern.png)

## Literature

Løvlid, D. H. (2011). *The Skjoldehamn Find in the Light of New Knowledge.* (Carol Lynn, Trans.). Lofotr Vikingmuseum. (Original work published 2009). (CC BY-NC-SA 3.0)


<!-- ### weitere: -->

<!-- Gugel kritisch und Fundnahe Rekonstruktion: -->
<!-- https://vikinghandcraft.blogspot.com/2020/03/gedanken-zur-skjoldehamn-gugel.html -->

<!-- # Weitere Sachen: -->

<!-- Nachfetten: -->

<!-- Das Fetten von Wollüberhosen mit einem Lanolin-Kurbad: -->

<!-- Verrühren Sie einen Teelöffel reines Wollfett mit heißem Wasser und geben 1-2 Tropfen Fein- oder Wollwaschmittel als Emulgator dazu (damit sich das Fett mit dem Wasser verbinden kann). Das Ganze gut verrühren, bis eine milchig-weiße Flüssigkeit entsteht. Diese Mischung geben Sie in einen Eimer oder großes Gefäs mit ca. 1-2 Liter warmem Wasser. Nun können Sie die frisch gewaschene Wollwindelhose in die Flüssigkeit einlegen. Lassen Sie die Wollhose ca. 2-3 Stunden (oder auch gerne über Nacht) im Imprägnierbad liegen. Sollte die Wollfaser zuviel Wollfett aufgenommen haben, kann das überschüssige Wollfett einfach mit lauwarmem Wasser ausgespült werden. -->

<!-- Tipp: Zuerst das Wollfett in kochendem oder sehr heißem Wasser lösen, dann 1-2 Tropfen Spüli, Fein- oder Wollwaschmittel dazu geben und dann auf 2 Liter auffüllen. So gelingt das Lösen des Wollfettes leichter. -->

