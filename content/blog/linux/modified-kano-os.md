---
title: "Modified Kano Os"
date: 2023-01-12T12:08:28+01:00
draft: true
---

# My Kano Os modification

## How to modify an image

1. Create a mountpoint:
    
    `mkdir <mountpoint>`

2. Find the root partition in the disk image:

`find <file.img>`

The partition we want would be the second one.

Take the startsector of the partition and multiply it by 512.

3. Mount the partition:

`sudo mount Kanux-Beta-v3.16.2-Lovelace-jessie-release-8gb.img -o offset=113246208 mountpoint`

or

`sudo mount Kanux-Beta-v3.16.2-Lovelace-jessie-release-8gb.img -o offset=$[512*<startsector>] mountpoint`

4. You may enter your mountpoint and edit the disk image.

## My changes to Kano-Os for the Raspberry Pi 2

### Language

```
$ sudo vim mountpoint/etc/default/locale

-- LANG=en_US.UTF-8
++ LANG=de_CH.UTF-8
-- LC_CTYPE=en_US.UTF-8
++ LC_CType=de_CH.UTF-8

```


