---
title: "Movies I own"
date: 2022-08-12T23:42:47+02:00
draft: false
---

Updated: 12/08/2022

- Science Fiction
  - Marvel Comics
    - The Incredible Hulk (2006 oder so)
  - DC
    - Superman Returns (2006)
  - Other
    - Watchmen
	- Back to the Future

- Fantasy
  - The Hobbit - An unexpected Journey - Extended Edition

- etc.
