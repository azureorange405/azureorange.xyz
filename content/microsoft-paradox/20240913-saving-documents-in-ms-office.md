---
title: "MS Office: Saving Documents"
date: 2024-09-13
draft: false
---

Whenever you have created a new file in a MS Office program, you typically want to save it somewhere on your machine.

From using Linux and GTK or whatever I'm totally used to two things happening when hitting the save button.
Either you already have the file saved somewhere and it just gets updated with the new content or 
there is no file path yet and your system file picker opens asking you where to save the file.

![save file prompts](./save-file.png)

On Windows or specifically MS Office, you first get these annoying dialogs always pointing to some random locations you don't want to save your file in.
The you hit the "more options" button and you get redirected to the save-as-menu of MS Office that just shows more options you don't use such as OneDrive (even though it's not installed).
You then have to hit the search file system button to get the file explorer you wanted from the start.

Well the annoying part is that you have to do this every time you safe a new file.
This results in thousands of frustrating and unnecessary button presses a month.

Just show me a file picker please. Or maybe even give me the option to customize the behaviour to my liking.

The people are able to deal with it, it's not that complicated.
