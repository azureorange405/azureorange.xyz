---
title: "DIY: Book Binding Attempt No. 1"
date: 2021-07-22T14:44:32+02:00
draft: false
---

A very good [friend](https://www.mcozzio.com/) of mine, well, has written a fantasy novel.                                     
So as his birthday was coming up and yet no printed version of his novel existed, I saw and took the opportunity to bind it in leather and gift it to him.

First, I went to choose the paper for the text block at the local stationary shop. Very soon, I realized that nicely textured paper colored in a pleasant looking warm white was extremely expensive. Because of the book not being in its final state and my budget being limited, I then went for a slightly thicker but standard 120 g / m2 printing paper.

![assembling the signatures](01-book.jpg)

After formatting the novel into signatures using Libreoffice, I then simply printed the whole novel and then folded the sheets using a bone folder to ensure a clean fold. After folding, I first assembled the signatures, then the text block and pressed it between two wooden plates using screw clamps. Ideally, a book press would have been used here, but I did not have one.

![pressing the text block](02-book.jpg)

Now it was time to mark the position of the sewing holes. To do so, five vertical lines were drawn onto the text block using a pencil, then the signatures were opened one by one and the sheets were pierced at the marked spot using an awl.

Using a black linen thread, I started cord stitching the text block. Again I had to improvise here, as I do not own a suiting stitching jig.

![stitching the text block](03-book.jpg)

After binding, I glued on the some black end sheets, the text block was put back into the press, and I glued the spine using normal PVA wood glue. After the glue dried, I tried to hammer the spine using a wooden mallet so it would take a rounded shape. This process did not work as well as expected and I think that is because I used way to much glue on the spine.

![glueing the spine](04-book.jpg)

Then it was time to sew the headbands, unfortunately, I did not take a picture of this step, but I used some black and brown wool for sewing and a piece of linen cord for the core. A little glimpse of the final headbands may be seen in the final picture though.

Now I have cut some front and back covers from 3 mm plywood and glued the cord used for binding onto the outside of these covers, then I cut a piece of leather with 3 mm in thickness and glued it onto the spine. To accomplish this, the leather was damped with some water, glued onto the spine using the same PVA glue as before and pressed around the cords using some more cord and the improvised press as a stand.

![leather cover](06-book.jpg)

The next day the leather was glued on top of the covers. After the glue dried, I carved a simple frame, as well as the title of the novel into the damped leather, which again proved to be more difficult than expected.

![carving the cover page](08-book.jpg)

After everything dried, the leather was stained using some brown antique leather stain. The edges were thinned out using a scalpel, folded over and glued onto the cover. Because of the leather still being very thick, the edges had to be fixated using some staples until the glue dried properly. Then, finally, the end sheets were glued to the cover.

![staining the leather](09-book.jpg)

The author had a very good conception of how the country in which his novel takes place looked like. 
Therefore, I took a sketch of the map, which he drew, redrew it on paper using a fine liner, and copied it onto normal 80 g / m2 printing paper. I trimmed the edges of the paper and glued the map onto the black end sheet to give the book a final touch.

![the map](07-book.jpg)

And this is how the finished book turned out.

![The finished book](book.jpeg)
