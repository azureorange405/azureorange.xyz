---
title: "To keep track of things:"
date: 2022-08-12T23:05:43+02:00
draft: false
---

## Some stuff I collect

- 📖 {{< linkpage page="/list/books" title="My personal library" >}}
- 🎞 {{< linkpage page="/list/movies" title="My movie collection" >}}
- 🎧 {{< linkpage page="/list/music" title="My music collection" >}}

## About GNU/Linux and Software in general

- 💾 {{< linkpage page="/list/software" title="Software I use and recommend" >}}

