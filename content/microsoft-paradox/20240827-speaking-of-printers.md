---
title: "Speaking of Printers"
date: 2024-08-27
draft: false
---

The way Windows handles printer drivers and print settings disturbes me big time.
If it's not enough that most applications do not ship a usable printer dialog.

Each printer has its own driver and as such its own print settings.

But why the hell do the applications settings and the drivers settings overwrite each other?

If I set the print to black and white but the driver settings have it set to color, I can always just hope for the best and gamble on which one will win.

On Linux I never ever had an issue with such a thing. 
Please Microsoft, just do it right for once.

