---
title: "OverlayFS: Merged Home Directory"
date: 2023-03-14T20:05:55+01:00
draft: true
---

I first created some test directories:

Then I mounted these as such:

```sh
# /etc/fstab

overlay /home/azure/union	overlay noauto,x-systemd.automount,xino=auto,lowerdir=/mnt/hdd/test-hdd,upperdir=/mnt/ssd/test-ssd,workdir=/mnt/ssd/test-work 0 0
```

created an rsync script to move unused files to the hdd:

```sh
rsync script (/etc/cron.weekly/overlay-sync)
```

then overlayed my home directories.

creted workdirs such as .Documents.

NOT YET: then moved rsync script to cron.weekly.


TODO:

What happens to the files in lower after a copy_up action? They stay and get "overlayed"


What happens to unfinished anacron jobs? (during shutdown e. g.)
What happens in the work directory?

Only copy_down at first, after some time (e.g. two months) do a checksome comparison and if match then delete upper file.
