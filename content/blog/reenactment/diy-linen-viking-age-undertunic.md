---
title: "DIY: Linen Viking Age Undertunic"
date: 2023-08-01
draft: false
---

There are fewer finds of Viking Men's clothing than Women's, because of textiles being preserved by proximity to metal or tannin (from wood) in a ground burial.
Many men in the pagan Viking Age though have been cremated rather than buried.
If buried, women were much more likely to be wearing jewellery made of metal and textiles in the immediate area of such jewellery had a chance of surviving time.
Men needed less jewellery to hold their clothings together, what resulted in less metal being present in their graves.
So there is less evidence on men's clothing and what has been reconstructed had to be scraped together from a few lucky findings.

Although many textiles were made of worsted wool, some were also made from (mostly undyed) linen.
This time I try to sew a linen undertunic, because it is much easier on the skin, isn't as warm and scratchy as wool.

## The textile choice

Undergarments were particularly made from linen and the archaeological evidence indicates that almost all of these textiles were in tabby or plain weave.
The tabby weave is made by threads at right angles going alternately over and under each other.

![tabby weave](tabby-weave.png)

I bought a piece of tabby weaved linen cloth from a great guy at the medieval market in ...,
which feels really nice on the skin and is very light, so it suits perfectly for a hot summer like we have this year.

![tabby weaved linen cloth](tabby-weaved-linen-cloth.jpg)

## The pattern

The patterns reconstructed for shirts or undertunics or smocks, however you want to call them, were all fairly similar to each other.
They mainly consist of a rectangular front and back body piece and two more or less rectangular arm pieces.

The tunics reconstructed from findings in Birka, Sweden, seemed to commonly have the front and back pieces cut in one piece with no seams on the shoulder lines and small round or keyhole shaped necklines.
On the sides they had some triangular gores inserted where the front and back panels meet.

Generally undertunics made of linen had longer sleeves and skirts than the overtunics made of wool.
This might have been to make the undertunic visible to show that one was wealthy enough to afford it, as linen was much more expensive at that time.

![Birka tunic](birka-tunic.png)

## The seam

There were many variations of flat-felled seams found from Viking Age garments.
These include a running stitch holding together the to pieces of fabric and then the seam allowances were folded over and tacked down.
As flat-felling works well for undergarments (it was less appropriate for outer garments), I decided to go with that.



#### I chose a backstitched one, because inside and I liked it for the Ärmel. The rest maybe with normal one?

## References

https://www.cs.vassar.edu/~capriest/viktunic.html
https://www.cs.vassar.edu/~capriest/mensgarb.html

https://www.youtube.com/watch?v=2FzvvJ_HZJM




### Brika Graves

Undertunics or Smocks
https://www.cs.vassar.edu/~capriest/mensgarb.html

viking tunic construction
https://www.cs.vassar.edu/~capriest/viktunic.html

### Thorsbjerg
Book: p. 73
viking-clothing_compress.pdf

## Viborg Shirt
https://www.vikingsof.me/downloads/clothing-guide/male.html#shirt

## 970 viking tunic
https://seamstrue.com/generators/970-viking-tunic/
https://www.livinghistoryarchive.com/article/mastering-the-look-your-complete-guide-to-viking-reenactment-tunics

### Tunic reconstruction Haithabu
https://projectbroadaxe.weebly.com/viking-age-nordic-history/viking-age-fashion-mens-type-i-tunic-from-10th-century-haithabu-hedeby

### Generic reconstruction
/home/azure/Simple-Viking-Clothing-for-Men.pdf

### About clothing
https://www.historyonthenet.com/viking-clothing-warm-and-durable
http://www.hurstwic.org/history/articles/daily_living/text/clothing.htm




### Maybe here something?
https://www.vidars-horde.de/en/pages/a-for-articles



## For other projects

### about bags
https://sagy.vikingove.cz/en/reconstruction-of-the-viking-bag/

### Brettchenweben?
https://alicjajaczewska.com/ix-xi-century/bands/reconstruction-of-birka-b17-pattern/
