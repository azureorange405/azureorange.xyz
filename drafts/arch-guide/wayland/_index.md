---
title: "Wayland"
date: 2022-11-30T18:31:04+01:00
draft: true
---

# Wayland

## Wofi as a Wayland Dmenu Alternative

Because of dmenu being an X11 application, I had to find an alternative to use under wayland.

For the moment I use [wofi](https://hg.sr.ht/~scoopta/wofi) to do exactly this.

I use my dmenu scripts and converted them to function with wofi by simply exchanging the run command.
But you could also write a function to check what display server you are on, that passes the right command to the script.
