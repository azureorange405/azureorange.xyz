---
title: "MS Office and minimized Windows"
date: 2024-09-06
draft: false
---

Speaking of deminimizing windows that are not in use ...

Just open some Word documents or spreadsheets in Excel if you wish.
About ten or so will do the trick.

Then minimize them all because you cannot work on everything simultaniously.

Not open a new MS Office document from the file explorer and watch what happens.

All the other windows are reopened! How and Why?!



The most disturbing part about this is what's to be found in the [Microsoft Forum](https://answers.microsoft.com/en-us/msoffice/forum/all/stop-word-from-maximizing-all-documents-when/76a4e0e4-3772-420b-a4e1-f06a2cd959cd)
if you search for a solution.

They know about the issue for several years now.
They tell you it is actively investigated.

...

Well it's not.

Thank you for nothing.


