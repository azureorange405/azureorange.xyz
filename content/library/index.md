
![library.gif](library.gif)

In here I hold a list of books and articles that might be interesting to read.
If freely accessible, I will attach the URL directly pointing to the pdf.
If not, I will link the page to where I found the book and maybe you're able to access it via your institution.

{{< rawhtml >}}<details><summary>Page Content</summary>{{</ rawhtml >}}
1. [Linux and Technology]({{< ref "#linux-and-technology" >}})
2. [Viking Reenactment]({{< ref "#viking-reenactment" >}})
3. [Miscellaneous]({{< ref "#miscellaneous" >}})

{{< rawhtml >}}</details>{{</ rawhtml >}}

## Linux and Technology

### Books and articles

{{< table id="sample" class="bordered" data-sample=10 >}}

| Author | Year | Title | URL | Citation |
| ------ | ---- | ----- | --- | -------- |
| Thompson Ken | 1984  | Reflections on Trusting Trust | [pdf](http://www.cs.cmu.edu/~rdriley/487/papers/Thompson_1984_ReflectionsonTrustingTrust.pdf) | [BibTex](thompson1984.bib) |

{{</ table >}}

### Websites

If you want to know about how anything on Linux works, I'd suggest you check out the ArchWiki.
They probably feature an article about the topic you are looking for.

[ArchWiki](https://wiki.archlinux.org)

## Viking Reenactment

### Books and articles

{{< table id="sample" class="bordered" data-sample=10 >}}

| Author | Year | Title | URL | Citation |
| ------ | ---- | ----- | --- | -------- |
| Arbman Holger | 1984  | Birka I: Die Gräber / Tafeln | [archive.org](https://archive.org/details/BirkaITafeln) [pdf](https://archive.org/download/BirkaITafeln/Birka_I_Tafeln.pdf) | [BibTex](arbman1984.bib) |
| Arwidsson Greta | 1984 | Birka II: Systemische Analysen der Gräberfunde: Untersuchungen und Studien | | [BibTex](arwidsson1984.bib) |
| Geijer Agnes  | 1938  | Birka III: Die Textilfunde aus den Gräbern | [archive.org](https://archive.org/details/BirkaIII) [pdf](https://archive.org/download/BirkaIII/Birka_III.pdf) | [BibTex](geijer1938.bib) |
| Svestad Asgeir | 2021 | Buried in Between: Re-interpreting the Skjoldehamn Medieval Bog Burial of Arctic Norway | | [BibTex](svestad2021.bib) |
| Gustin Ingrid | 2021  | Means of Payment and the Use of Coins in the Viking Age Town of Birka in Sweden - Preliminary Results | [publicera](https://publicera.kb.se/csa/article/view/997) [pdf](https://publicera.kb.se/csa/article/download/997/949) | [BibTex](gustin2021.bib) |
| Ewing Thor | 2007 | Viking Clothing | [vdoc.pub](https://vdoc.pub/documents/viking-clothing-4n0b6mo3mc40) | [BibTex](ewing2007.bib) |
| Raffield Ben | 2019 | Playing Vikings | [uchicago](https://www.journals.uchicago.edu/doi/10.1086/706608?fbclid=IwAR1ZOhcJ24u8552KL4ySzhJW6YjE4vjW3zCfKJvrrbHFUx4I5U5PoyGi1Kw#) [pdf](https://www.journals.uchicago.edu/doi/pdf/10.1086/706608?download=true) | [BibTex](raffield2019.bib) |

{{</ table >}}

### Websites

If you are interested in writing in Viking Age runes aka the Younger Futhark, then this website will be featuring one of the best guides I have ever seen. And behold, what seems to be easy on first sight, often is more complicated that you thought.

[The Viking Rune](https://www.vikingrune.com/) / [How to Write in Old Norse With Futhark Runes: The Ultimate Guide](https://www.vikingrune.com/write-in-futhark-runes-old-norse-guide/)

---

The Viking Age Compendium contains our ongoing research into the material culture of Viking Age Britain. The Compendium details the kinds of items and clothing worn or used by people living in, or possibly visiting, Britain between AD 800 and 1100.

[The Viking Age Compendium](http://www.vikingage.org/wiki/wiki/Main_Page)

---

A well researched collection on Viking Age reenactment by Þóra Sharptooth.

Welcome to my filing cabinet! Here you will find links to several articles I have written on subjects of interest to Viking Age re-enactors.

[Viking Resources for the Re-enactor](https://www.cs.vassar.edu/~capriest/vikresource.html)

## Miscellaneous

This section lists all the books I own that don't fit into any of the other categories.

{{< book "how-to-stop-time" "10" >}}
{{< book "before-the-coffee-gets-cold" "100" >}}
{{< book "harry-potter-1" "100" >}}
{{< book "harry-potter-2" "100" >}}

