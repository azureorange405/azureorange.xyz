---
title: "MS Word: Layout Settings and Sheet Formatting"
date: 2024-09-11
draft: false
---

I wanted my pupils to format a Word document in DIN A3.

The paper format A3 did not show up neither in the format list nor in the extended format settings dialog.

I started to explain my 10 years old pupils how big a DIN A3 sheet is and how they may use custom layout settings.

Later when I had some time, I searched the internet for a solution to permanently add new layout settings.

Then I found this: The layout settings are DIRECTLY LINKED TO THE PRINTER SETTINGS!

You cannot set a sheet format that is not supported by your selected printer.
You first have to select something like MICROSOFT PRINT TO PDF to be able to use more format settings.

Now I really wonder what Microsoft developers get paid if they program such stupid things.

Everyone, go and use [LibreOffice](https://libreoffice.org/) now ...

