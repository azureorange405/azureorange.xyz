---
title: "DIY: Tardis 3d-Printing"
date: 2024-01-16T14:51:24+01:00
draft: false
---

I just finished my first ever 3d model using [OpenSCAD](https://openscad.org/).

The programming aspect to 3d modelling is quite interesting and something I get along with very well.

## The first version

I did use an image with the outer measurements of the Tardis as a reference guide
(I cannot share the image here, but you'll find it by searching the internet  for "Tardis measurements").
I calculated the percentages for all lengths relative to the width of the main body.
This way I can change the scaling of my model later on.

![tardis-v1-openscad-screenshot](./tardis-openscad.png)

For now:

- the text is somewhat off as I used the wrong font and its scale was just estimated
- the rectangular cutouts are all the same and positioning was only estimated
- the roof's lengths are estimates as well
- the lamp at the top is very much simplified

You'll find all corresponding files on my [GitLab](https://gitlab.com/azureorangexyz/openscad-projects/-/tree/main/tardis?ref_type=heads).

In the near future I will fix the lackings described earlier and try to print it as well.

