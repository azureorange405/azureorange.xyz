---
title: "Explorer.exe: File Explorer and Window Manager"
date: 2024-09-19
draft: true
---

Well I have no idea what the explorer.exe task does and does not include.
But I know if I restart the process, my desktop wallpaper may change, the task bar resets and well the whole system is gone until it restarted.

I know most of the time this does not matter to the user.
But I tend to use tabs a lot. I hoard tabs in my browser and I do have many in my file explorer (mainly because it's so hard to navigate).

And from time to time Windows tends to hang up or crash completely.
That's very normal for an operating system and everyone is used to it (or is it).

But if you then restart the crashed explorer task, that should have nothing to do with your system hanging,
unless it does because it is half your system, you lose all your carefully placed tabs and your workflow is reset as well.

I know I'm the only one with this problem, but please Microsoft, one process per process and one task per task.

It's not that hard (I guess).
