---
title: "Diy Elder Wand"
date: 2024-01-13T02:05:01+01:00
draft: true
---

![cinematic-elder-wand](./diy-elder-wand/cinematic-elder-wand.jpg)

printed in five parts
![3d-print](./diy-elder-wand/3d-print.jpg)

look at this ugly mess --> cutter, sanding paper and hundreds of hours
![3d-print-closeup](./diy-elder-wand/3d-print-closeup.jpg)

sprayed brows, painted details
![glossy-finish](./diy-elder-wand/glossy-finish.jpg)

matte finish sprayed
![finished-elder-wand](./diy-elder-wand/finished-elder-wand.jpg)

core and hidden message inside.
