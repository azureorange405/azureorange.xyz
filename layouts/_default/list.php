{{ define "main" }}
  {{ if .Content }}
    <div class="index-content {{ if .Params.framed -}}framed{{- end -}}">
      {{ .Content }}
    </div>
  {{ end }}
<content>
  {{ if .Data.Singular }}
    <h3 class="blog-filter">{{ i18n "filtering-for" }} "{{ .Title }}"</h3>
  {{ end }}
  <h3>Posts</h3>
  <ul class="blog-posts">
    {{ range where .Pages "Draft" false }}
    <li>
      <span>
        <i>
          <time datetime='{{ .Date.Format "2006-01-02" }}' pubdate>
            {{ .Date.Format (default "2006-01-02" .Site.Params.dateFormat) }}
          </time>
        </i>
      </span>
      {{ if .Params.link }}
        <a href="{{ .Params.link }}" target="_blank">{{ .Title }}</a>
      {{ else }}
        <a href="{{ .Permalink }}">{{ .Title }}</a>
      {{ end }}
    </li>
    {{ else }}
    <li>
      {{ i18n "no-posts" }}
    </li>
    {{ end }}
  </ul>
  <h3>Upcoming posts</h3>
  {{- range first .Site.Params.FuturePostLimit ( where .Pages "Draft" true ) -}}
    <li>{{.Title }}</li>
  {{ else }}
    No posts
  {{ end }}
  {{ if not .Data.Singular }}
    <div>
      {{ range .Site.Taxonomies.tags }}
        <a class="blog-tags" href="{{ .Page.Permalink }}">#{{ .Page.Title }}</a>&nbsp;&nbsp;
      {{ end }}
    </div>
  {{ end }}
</content>
{{ end }}
