---
draft: false
---

![contact.gif](contact.gif)

If you don't expect a response and just want to add something for everybody, why don't you use my [guestbook](https://azureorange.xyz/guestbook)?

## The 90s way

- [contact@azureorange.xyz](mailto:contact@azureorange.xyz)

You may contact me via my email address, but don't expect me to reply right away, especially if:

- you want to help me improve my web design (I like it this way)
- you want to sell me something

These are the only emails I got here for quite some time, so I'm not checking my inbox regularly I guess.

## The 21st century way

I am on various social media platforms such as:

- [Mastodon](https://fosstodon.org/web/@azureorange)
- [PixelFed](https://pixelfed.social/azureorange) (just me enjoying stuff)
- [Odysee](https://odysee.com/@AzureOrange404:7) (no content though)

