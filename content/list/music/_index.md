---
title: "Music i own"
date: 2022-08-12T23:42:56+02:00
draft: false
---

Updated: 12/08/2022

This is all the music I own on CD:
- Metal
  - Metallica - Stranger
  - Metallica - Nothing Else Matters
  - Metallica - Load
  - Metallica - ReLoad
  - Metallica - Until It Sleeps
  - Apocalyptica - Plays Metallica by four cellos
  - Whitesnake - 1987
  - Iron Maiden - The Book of Souls
  - Iron Maiden - A Matter of Life and Death
  - Korn - Some new studio album (the white one)
  - Lamb of God - Sacrament
  - Alestorm - Captain Morgan's Revenge
  - Alestorm - Back Through Time
  - Alestorm - Black Sails at Midnight
  - Five Finger Death Punch - The Wrong Side of Heaven and the Righteous Side of Hell
  - Five Finger Death Punch - Got Your Six
  - Eluveitie - slania
  - Arch Enemy - The Root of all Evil
  - Equilibrium - Armageddon
  - Shinedown - Amaryllis
  - Chimaira - Resurrection
  - Ozzfest Live 2002
- Country
  - Johnny Cash - Man in Black
  - Country Giants - Volume One
- Other
  - Bon Jovi - Crush
  - Bon Jovi - Lost Highway
  - Bon Jovi - Keep the Faith
  - Bon Jovi - 7800° Fahrenheit
  - Bon Jovi - Blaze of Glory / Young Guns II
  - Bon Jovi - Mercury
  - Bon Jovi - New Jersey
  - [more to come]
---
This is all the music I own on LP:
- Metal
  - Amon Amarth - Berserker
  - Amon Amarth - The Pursuit of Vinkings
  - Jinjer - Wallflower
- Deutschrock
  - FreiWild - Still II Single LP
  - Brüder4Brothers - Brotherhood
- Other
  - [even more]

--- 
This is all the music I own on tape:
- Country
  - Nicky James - Cowboy At Heart
  - Willie Nelson - Country Heroes
  - Boxcar Willie - The Collection
  - Jeff Turner - Rolling Wheels
  - Cherokee - Counrty & Western
  - Mark Middler - On the Rebound
  - From Nashville to Tennessee - Number 1 Country Hits
  - Jimy Hofer - Simple Songs
  - Jimy Hofer Band - Better Letter
  - Country & Western Festival - Volume 1
  - Truckdriving Songs 6
- Other
  - Garth Brooks - The Chase
  - Garth Brooks - In Pieces
  - Pia - When the Lights go out
  - Betty Boo - Boomania
- And then there's this one cassette which has "Only You" by "The Flying Dickets" on it. (In an endless loop!)
