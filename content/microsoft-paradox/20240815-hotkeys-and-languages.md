---
title: "Hotkeys and Languages"
date: 2024-08-15
draft: false
---

In every graphical word processing software I've ever used you find hotkeys to enhance your workflow.
This does not exclude Microsoft Word, which actually is a good thing.

If I wanted to format some text bold using hotkeys, normally I'd use ctrl+b on my keyboard.
This actually works the same way in MS Word.

But ...

Only if your system language and as such the language of MS Word is set to english.
If you change said language, the hotkeys change as well.

I will never understand why someone thought this is a good thing to do.

If e.g. you have set the language to german -- such as in my school computer and which cannot be changed due to the organizational setup --
the hotkey changes from being ctrl+b(old) to ctrl+f(ett).

Oh no, wait.

Ctrl+f is already in use by a not really necessary function, it has to be ctrl+shift+f

So not only is the letter "translated" into the system language and I have to think about how to spell the word in every language I encounter,
no, the whole structre of the hotkey changes as well!

### Edit 2024-08-28

Well it seams the story has not ended.

As I mentioned the hotkeys change depending on the system language set.
But wait! This only effects the desktop version of the application.
In the cloud based version the hotkey will never change.

Now even if you always work in environments set up in the same language, you still have to remember multiple hotkeys for the same action!

I now believe MS developers don't get paid for their work, I cannot think of another explanation for this.
