---
title: "Displays and minimized Windows"
date: 2024-09-04
draft: false
---

In school I happen to change the attached display and the way it is connected all the time.

Sometimes I want to show something on my smartboard, then I want it to be disconnected because I write emails or other things.

Every time I change displays though (switching on the beamer etc.) random applications get deminimized.

I minimized them for a reason, and the reason is I'm not working on these windows at the time.
Leave them be that way, it cannot be that hard for god's sake!

