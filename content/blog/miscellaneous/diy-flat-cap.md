---
title: "DIY: Sewing a Flat Cap"
date: 2021-12-03T15:13:18+02:00
draft: false
---

I always wanted to wear a flat cap, and now as I had the right materials laying around, as well as some spare time, so I started working on this project.

I have found a suiting pattern in the internet archive and redrew it to create a pattern with less error, because of the original being drawn very vaguely.
You may find the pattern attached both as a .PDf and as a .SVG.
The pattern suited for hat size 58, as this approximatly was mine, I started right away.

[Download pattern](pattern.tar.gz)

The materials I have used were 60 x 60 cm of a rather large-meshed jute fabric for the outer layer, approx. 60 x 60 cm of a patterned cotton fabric for the inner layer, some mixed fabric for the brim and the head band, which just had to match the color style of the others and a piece of leather for the brim with a thickness of app. 3 mm.

First I had to cut out all the pattern pieces, so I pinned the printed pattern to my fabrics and roughly cut them out, leaving some space for a seam, of course.

![inner layer](04-flatcap.jpeg)

Onto the jute pieces I ironed some fusible interlining for stability and to kind of protect the inner fabric from getting all waxy on hot summerdays.

![inner layer](01-flatcap.jpeg)

This brings me to my next step, soaking the jute in bees wax, to give it some water repellant functionalities.
The jute was placed on the ironing board and some wax pellets were evenly dirstributed on top.
Then I simply ironed over it until the wax melt and soaked into the fabric.

For making the brim, I took a piece of thick leather and some fabric, which I sewed together using a sewing machine.

![inner layer](02-flatcap.jpeg)

Finally it was time to sew everything together.
I had to sew the whole cap by hand, because I did not think that my sewing machine could handle the waxed fabric.

![inner layer](06-flatcap.jpeg)

After many long minutes of sewing, I attached the shield, the inner lining and finally the head band, which nicely covered all my ugly stiches.

![inner layer](07-flatcap.jpeg)

As I have finished the flatcap, I realized that it was a little to large to fit my head tightly.
Because of me being really tired, I simply took some strong linen thread and stiched around the cap and pulled it tight. 
Now the flatcap fits perfectly.

Here a picture of the finished flatcap and the pattern:

![The finished cap](flatcap.jpeg)
