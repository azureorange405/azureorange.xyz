---
title: "Microsoft and their Love for Edge"
date: 2024-09-02
draft: false
---

I installed the brave browser on my work machine at school and wanted to use it as my main browser.
Changing the default browser is very straight forward.
Just hop into the Windows settings and change it.

Done? Nope, sorry.

Now you have to manually change every file type extension's settings seperately from Edge to Brave.
I really don't ever want an Edge window to open on my machine.

Now it should be fixed and I should never ever see Edge popping up again right?

Well no.

I found that if I follow a hyperlink from MS Teams it always opens the webpage in Edge and as of now I have not found a way to change this.

Thank you Microsoft for your subbornness.

