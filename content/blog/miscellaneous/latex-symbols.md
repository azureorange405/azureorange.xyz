---
title: "Latex: Symbols"
date: 2024-04-08T17:42:12+02:00
draft: false
---

This is how to do various symbols in LaTex.

## Text

### Standard LaTex

- … ellipsis (de: Auslassungszeichen) `\ldots`
- ◦ circle (de: Kreis) `\circ`

### Additional Symbols

`\usepackage{pifont}`

## Maths

