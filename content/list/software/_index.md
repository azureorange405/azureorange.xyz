---
title: "Software I recommend and use"
date: 2023-02-11T17:06:00+02:00
draft: false
---

Here provided you will find a list of the Software I use daily (or not) and which I can strongly recommend checking out.

- window managers
  - [xmonad](https://xmonad.org/)
  - [dwm](https://dwm.suckless.org/)
    - [my build](https://gitlab.com/azureorange404/dwm)

- terminal emulators
  - [kitty](https://github.com/kovidgoyal/kitty)
  - [st](https://st.suckless.org/) 
    - [my build](https://gitlab.com/azureorange404/st)

- browsers
  - qutebrowser
  - brave
  - firefox

- unordered (yet)
  - [dmenu](https://dmenu.suckless.org/) 
    - [my build](https://gitlab.com/azureorange404/dmenu)
  - [GIMP](https://www.gimp.org/)

- selfhosted (with docker or without)
  - [nextcloud](https://nextcloud.com/)
  - [jellyfin](https://jellyfin.org/)

- on my Android device
  - On [F-Droid](https://test.f-droid.org/) (app store)
    - [KISS Launcher](https://f-droid.org/en/packages/fr.neamar.kiss/)
    - [AnySoftKeyboard](https://f-droid.org/en/packages/com.menny.android.anysoftkeyboard/)
    - [Aegis Authenticator](https://f-droid.org/en/packages/com.beemdevelopment.aegis/)
    - [Collabora Office](https://www.collaboraoffice.com/releases-en/collabora-office-on-mobiles-supporting-password-protected-documents-and-available-on-f-droid/) (Office Suite)
    - [deedum](https://f-droid.org/en/packages/ca.snoe.deedum/) (A browser for the gemini protocol)
    - [Feeder](https://f-droid.org/en/packages/com.nononsenseapps.feeder/) (RSS Feed Reader)
    - [Jami](https://f-droid.org/en/packages/cx.ring/) (Communication Platform / Messenger)
    - [K-9 Mail](https://f-droid.org/en/packages/com.fsck.k9/)
    - [LBRY](https://f-droid.org/en/packages/io.lbry.browser/) (lbry / odysee client)
    - [mpv-android](https://f-droid.org/en/packages/is.xyz.mpv/)
	- [MuPDF Viewer](https://f-droid.org/en/packages/com.artifex.mupdf.viewer.app/)
    - [Syncthing](https://f-droid.org/en/packages/com.nutomic.syncthingandroid/)
	- Games
	  - [Antimine](https://f-droid.org/en/packages/dev.lucanlm.antimine/) (Minesweeper)
	  - [Mines3D](https://f-droid.org/en/packages/cos.premy.mines/) (A new approach to Minesweeper)
  - [Signal](https://signal.org/install) (Messenger)

- If you'd like to see more such {{< linkpage page="/list" title="lists:" >}}
  - {{< linkpage page="/list/books" title="books" >}} I own
  - {{< linkpage page="/list/movies" title="movies" >}} I own
  - {{< linkpage page="/list/music" title="music" >}} I own
