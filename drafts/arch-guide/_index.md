---
title: "Arch Guide"
date: 2022-11-30T18:20:58+01:00
draft: true
---

# Arch Linux Guide

In this section I will note some things, I had to do to my system to make it work in the way I wanted it to.

Maybe something in here will help you with your Arch or Linux journey.

## Content

- [Wayland](./wayland)
    - [configure newm](./wayland/newm)
    - [Suspend on Systemd and NVIDIA](./wayland/suspend-systemd-nvidia)
