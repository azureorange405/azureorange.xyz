---
title: "Suspend on Systemd and NVIDIA"
date: 2022-11-30T18:31:18+01:00
draft: true
---

I happend to experience some bugs in suspension behaviour after switching to wayland.

If I suspended my system using `systemctl suspend` the system would suspend and wake up as expected.

But if I left the system suspended for more than approx. 10 - 15 minutes it would not wake anymore. 
Or more precisely it would wake up, the screen flashes and then it immediately gets suspended again.

To ensure that suspension worked under wayland as well, I set the following values in `/etc/systemd/logind.conf` (For the moment I'm not shure if this was necessary at all):

```
HandleSuspendKey=ignore
HandleLidSwitch=ignore
IdleAction=ignore
```

Then I had to enable some systemd services to get the gpu-suspension right:

```sh
$ systemctl enable nvidia-suspend.service
$ systemctl enable nvidia-resume.service
```

