---
title: "The \"Microsoft Paradox\" series"
date: 2024-08-15T22:28:06+02:00
draft: false
---

# The "Microsoft Paradox" series

A mini blog where I share a series of observations and question the popularity of Microsoft software.

I've been a Linux user for about 8 years now.
As I started to work as a teacher, I now have to work on Windows machines and I do not like it.
I will write about anything that bothers me and maybe you had some similar experiences.

Please have a look at my short rants and maybe, just maybe, you start to see the flaws as well. 
