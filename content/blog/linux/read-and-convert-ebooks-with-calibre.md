---
title: "Read and Convert Ebooks With Calibre"
date: 2023-10-06T10:52:01+02:00
---

The commonly known default for reading ebooks probably is Adobe Digital Editions which is a proprietary tool that to me seems rather unmaintained.
A better alternative would be the [calibre ebook management software](https://calibre-ebook.com).

## Reading ebooks

By default calibre opens your books in the default program for the respecting file type.
Via the context menu you can choose to view it with calibre's inbuilt ebook viewer.

I personally use [zathura]() to open all my ebooks with.
Zathura is a powerful and highly customizable pdf and ebook viewer with vim-like keybindings.

## Converting ebooks

Adobe's acsm format and others may be encrypted and a plugin is needed to convert these.
I use noDRM's [DeDRM_Tools](https://github.com/noDRM/DeDRM_tools) and Leseratte10's [Calibre ACSM Input plugin](https://github.com/Leseratte10/acsm-calibre-plugin) to do this for me.
This way I can read all ebooks using calibre and convert them into pdfs which will ensure that I do not loose access after my renting time expired.

## Further information

For more read the [calibre manual](https://manual.calibre-ebook.com/).
