---
title: "Endless Shutdown"
date: 2024-08-22
draft: false
---

I don't know what's wrong with Windows 11 or if it's just the HP laptops we use in school.
Sometimes (well 8 out of 10 times) my pupils laptops take about 10 - 20 minutes to shut down.
And if I'm very lucky I even find them the next morning still "shutting down".

I advised my pupils to always close all the processes they used, because I thought maybe that's the problem.
Well it didn't change a thing and I have dozens of laptops standing on the desks waiting for the sun rising in the west before they finally shut down.
However you can always "fix" the problem by holding down the reset button for ten seconds.


Another thing I'd like to mention here are forced updates on startup.
How should anyone do serious work in school with these?

Every third student of mine has to wait for about ten minutes before they can use their laptops. Daily. What the hell.
