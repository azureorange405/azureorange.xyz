---
title: "Dmenu script: scanning with SANE"
date: 2022-09-28T15:27:30+02:00
draft: true
---

## dm-scan

A dmenu script which uses the sane function `scanimage` to search for configured and online scanners and lets you choose one to use.

The script then prompts you for some input (all via dmenu), such as format and save path, and finally it again uses `scanimage` to scan a document via your scanner.

