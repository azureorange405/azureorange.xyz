<div>
    <h2>Leave a Message</h2>
    <form action="" method="post">
        <input class="box" type="text" id="name" name="name" placeholder="Enter your name."><br><br>
        <textarea class="box" id="message" name="message" placeholder="Enter your message."></textarea><br><br>
        <input class="button" type="submit" name="submit" value="Submit">
    </form>
    <div>
        <h2>Messages</h2>
        <div id="messages">
            <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                $name = htmlspecialchars(trim($_POST['name']));
                $message = htmlspecialchars(trim($_POST['message']));
                $date = date("Y-m-d");
                $rname = "";
                $rmessage = "";
                $rdate = "";
    
                if (empty($name)) {
                    $name = "anon";
                }
                if (!empty($message)) {
                    if (file_exists('data.json') && filesize('data.json') > 0) {
                        $data = json_decode(file_get_contents('data.json'), true);
                    } else {
                        $data = array();
                    }
    
                    $flag = true;
                    foreach ($data as $submission) {
                        if (strcasecmp(trim($submission['name']), trim($name)) == 0 && 
                            strcasecmp(trim($submission['message']), trim($message)) == 0) {
                            $flag = false;
                            break;
                        }
                    }
    
                    if ($flag) {
                        array_unshift($data, array("name" => $name, "message" => $message, "date" => $date, "rname" => $rname, "rmessage" => $rmessage, "rdate" => $rdate));
                        file_put_contents('data.json', json_encode($data, JSON_PRETTY_PRINT | LOCK_EX));
                    }
                }
                if (isset($_POST['reply_to'])) {
                    $reply_to = intval($_POST['reply_to']);
                    $rname = htmlspecialchars(trim($_POST['rname']));
                    $rmessage = htmlspecialchars(trim($_POST['rmessage']));
                    $rdate = date("Y-m-d");
                
                    if (!empty($rmessage)) {
                        // Load existing data
                        if (file_exists('data.json') && filesize('data.json') > 0) {
                            $data = json_decode(file_get_contents('data.json'), true);
                        } else {
                            $data = array();
                        }
                
                        // Add reply to the corresponding message
                        if (isset($data[$reply_to])) {
                            $data[$reply_to]['rname'] = $rname;
                            $data[$reply_to]['rmessage'] = $rmessage;
                            $data[$reply_to]['rdate'] = $rdate;
                            file_put_contents('data.json', json_encode($data, JSON_PRETTY_PRINT | LOCK_EX));
                        }
                    }
                }
            }
    
            $json = file_get_contents('data.json');
            $data = json_decode($json, true);
    
            if (!empty($data)) {
                foreach ($data as $index => $item) {
                    echo '<div class="comment">';
                    echo '<p><small>' . htmlspecialchars($item['date']) . '</small><br><strong>' . htmlspecialchars($item['name']) . '</strong>: ' . htmlspecialchars($item['message']) . '</p>';
                
                    // Display existing reply if available
                    if (!empty($item['rname']) || !empty($item['rmessage'])) {
                        echo '<div class="reply">';
                        echo '<p><small>' . htmlspecialchars($item['rdate']) . '</small><br><strong>' . htmlspecialchars($item['rname']) . '</strong>: ' . htmlspecialchars($item['rmessage']) . '</p>';
                        echo '</div>';
                    }
                
                    // Reply Form
                    echo '<details><summary>reply</summary>';
                    echo '<form method="POST" action="" style="display: flex; flex-direction: column; align-items: flex-start;">';
                    echo '<textarea name="rmessage" placeholder="Your Reply" required rows="3" style="margin-bottom: 10px; width: 100%; resize: none;"></textarea>';
                    echo '<div style="display: flex; width: 100%;">'; // A div to hold the name input and button
                    echo '<input type="text" name="rname" placeholder="Your Name" required style="margin-right: 10px; flex: 1;">';
                    echo '<button type="submit">Submit Reply</button>';
                    echo '</div>';
                    echo '</form>';
                    echo '</details><hr>';
                
                    echo '</div>';
                }
            }
            ?>
        </div>
    </div>
</div>
