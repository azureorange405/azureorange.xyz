---
title: "Miscellaneous"
date: 2023-06-20T14:39:58+02:00
draft: false
---

![flaming cauldron](cauldron.gif)

Everything that does not fit into the other categories will be posted in here.
