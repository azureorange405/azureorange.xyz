---
title: "dmenu script: YouTube Music frontend"
date: 2023-07-30T18:59:40+02:00
draft: false
---

Wouldn't it not be great to listen to Music on YouTube without ever opening your browser?

I asked myself the same question and started experimenting with `mpv` and `yt-dlp`.
The script uses `dmenu` as its GUI.

In the main menu one may choose to browse local playlists and followed albums; search YouTube; configure some settings.

## Usage

Install the script to your preferred location and run it like normally.

```sh
dm-music:

    usage:      dm-music [OPTIONS]

    options:
        -h      display this help message
        -c      source a custom config file (overrides default locations)

    how to configure playlists:
        1)  Use the search functionality from the main menu or the search menu,
            then choose "add to playlist" and choose a playlist or create
            a new one.
        2)  create a text file in the playlist directory (default:
            /home/azure/.local/dm-music/playlists/) and add the song url
            one entry per line.
            Pattern: <url> <description>

    how to follow albums:
        1)  Create a text file (e. g. per artist) in the albums directory
            (default: /home/azure/.local/dm-music/albums/) and add the album url
            one entry per line.
            Pattern: <url> <description>

    config:
        default locations (in this order):
            1) /home/azure/.config/dmscripts/dm-music.conf
            2) /home/azure/.config/dm-music.conf
            3) /home/azure/.local/share/dm-music/dm-music.conf
            4) The config file of distrotubes dm-scripts
               will be sourced if available.

        These variables are intended to be configured in a custom config file:
            _data_path="/home/azure/.local/share/dm-music"
            _search_length=10
            _dmenu_cmd="dmenu -i -c -l 20"
            _mpv_cmd="mpv --no-video"
            _repeat=0   # 0=no-repeat, 1=repeat
            _random=0   # 0=not-random, 1=random



```

### YouTube search
The YouTube index can be searched from withing the main menu as well as the search menu.
While in these menus, a search history will be shown, which is saved locally in a history file.

The search is powered by `yt-dlp` and may take one or two seconds to complete.

In the configuration file you may specify, how many search results are fetched from YouTube.

### Playlists
You may configure local playlists to listen from.
Songs can either be added via the search menu or manually by editing the playlist files.

### Following Albums
Albums can be followed and played as a whole and do not need to be added to a playlist song by song.
At the moment this can only be done by adding the album's url to a local album file, as the search does not only return songs.

### Player Settings
From withing the main menu you may turn on / off the settings for repetition and randomness.
If turned on, the current playlist will be repeated or played in random order respectively.

![main menu](dm-music-main-menu.png)

These settings can be made persistent by editing the configuration file.

You’ll find the script on my [GitLab](https://gitlab.com/azureorangexyz/dmenu-scripts/-/blob/main/dm-music) and you are free to suggest any changes or improvements.

