---
title: "Aaron's Website"
date: 2022-08-12T22:28:06+02:00
draft: false
---

I am Aaron as well as a primary school teacher and this is my website.

In my free time I learn about CLI programs, shell scripting and some programming in various programming languages.
The shell scripts and programs I write are all available on my [GitLab](https://gitlab.com/azureorangexyz) for everyone to use.
I also started doing some web development (mostly frontend though) and some server administration for personal stuff (e. g. cloud and streaming services).
My websites are on GitLab as well if you want to contribute to some of the projects.

These are my websites (for now in german only):

- [DingsBox](https://dingsbox.schoolstuff.ch)
- [Linux macht Schule (my thesis)](https://linux.schoolstuff.ch)
- [SchoolStuff](https://schoolstuff.ch)

Another side of me enjoys doing viking reenactment and following various crafts.
In my blog I'll write about some of the things I'm crafting and maybe some day about the reenactment itself.

PS: I won't check for spelling mistakes as I only do this for fun.
So if you find some, keep them to yourself -- I don't mind.
