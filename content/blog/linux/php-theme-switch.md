---
title: 'PHP: Theme Switch'
date: 2024-01-26
draft: false
---

On one of my [school related websites](https://dingsbox.schoolstuff.ch) I had to implement a theme toggle because some of my collegues did not like my dark theme.

This toggle allowed for choosing between a dark and a light theme using some javascript running in your browser.

This week I had the glorious idea to implement a backend theme toggle switch using some php. I mean without anything else running in my backend, I cannot identify any of you guys. That simply means that if you switch the theme, everybody gets it.

I thought of this being a fun easter egg, so, somewhere on my website there is a button for you to switch everyone's theme from gruvbox to solarized dark.

Have fun and some patience as well (there is some server side caching going on) when toggling the theme.
